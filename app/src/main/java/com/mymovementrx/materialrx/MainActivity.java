package com.mymovementrx.materialrx;

import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // instantiate toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        // set toolbar as actionbar
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();

        if( actionBar != null ) {
            actionBar.setTitle("Movement Rx");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // Material design APIs
                actionBar.setElevation(10f);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String title = (String) item.getTitle();
        Toast.makeText(MainActivity.this, title, Toast.LENGTH_SHORT).show();

        switch (item.getItemId()) {

            case R.id.profile :
                break;

        }

        return true;
    }
}
